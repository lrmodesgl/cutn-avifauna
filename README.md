# cutn-avifauna

Student Sakthi-Narpavi's(M.Sc. Life Sciences) attmept in creating a checklist, using free and open platform technologies for data collection, GIS/RS, Report generation, Visualization, Sharing and Publication in openaccess.

# Tools & Workflows

Deployed Tools and Workflows for the field observation plays a vital role in quality delivery of the project.
Tools used must adhere to the scientific principles and practices. Based on such philosophy the survey was constructed with the tools with following qualities:

1. Transparency
2. Collaborative Contribution
3. Social Auditing & Review
4. Availability
5. Accessibility
6. Affordability
7. Open Data Access policy (Repetability)
8. Open Access publishing policy

Since Ornithology studies/surveys require interdisciplinary approach, in several key areas such as :

1. Field Survey & Observation
 *. Data Collection
 *. Data Acquisition
2. Data Storage & Management 
3. Data Analytics (Statistics, Pre & Post processing)
 *. Data conditioning
 *. Identification
 *. Classification, Grouping
4. Making Sense of Data - Data Visualization, Exploration
 *. Interactive exploration
 *. Filtering
5. Report Generation
6. Sharing
7. Publishing

Data can be collecting photos, locations, recording species & familty names, numbers, etc... All such and other tasks need to be supported by tools & frameworks developed with above said scientific principles and practices. Several softwares such as :

1. OSMAnd          -       Location based field data collection
2. JOSM            -       Arm chair mapping
3. QGIS            -       GIS processing, Statistics, Atlas generation
4. Libreoffice     -       Documenting reports
5. Photocollage    -       Preparing collage of collected bird photos
6. Inkscape, GIMP  -       Editing Vector & Raster images
7. Git             -       Version control system for democratic collaboration

that are collaboratively developed mostly with [free software design principles](https://www.gnu.org/philosophy/free-sw.en.html) and released for usage with [copy left licenses](https://www.copyleft.org/). The collected, processed data & report are also released under [creative commons license](https://creativecommons.org/). These tools, and licenses around them nurtures practicing citizen science, which is a fundamental aspect with respect to the field of ornithology itself. Since its inception in India, data has been collected, validated and moderated to biodiversity count with the contribution of several thousands of citizens with different backgrounds. Both contribution and tools used determine the ownership of the data collected, so that enclosure for extortion in future can be prevented.

Especially, with the Volunteered GIS, systems like Open Street Maps, this project initiates the formal contribution along with validation of the CUTN campus for the first time. There is big difference in mapping a educational institution in map services like google, apple from mapping the same place through OSM with the help of tools like JOSM. This fundamentally changes the course of data updation and maintanance method and reusing without any commercial blocks for educaitonal, research or social good purposes. All the data contributed to OSM are licensed under [Open Data base license](https://wiki.openstreetmap.org/wiki/Open_Database_License).

Using such alternative softwares, hardware, frameworks and tools wherever possible has been a important decision of the project right from the beginning and it has been successfully demonstrated so far. I believe such practice need to be further followed up by any scholar who works in similar area of research or even for continuing this project.